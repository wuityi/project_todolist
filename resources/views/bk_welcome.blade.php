<!doctype html>
<html lang="{{ app()->getLocale() }}">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <title>Laravel</title>

    <!-- Fonts -->
    <link href="https://fonts.googleapis.com/css?family=Raleway:100,600" rel="stylesheet" type="text/css">
    <link rel="stylesheet" href="css/app.css">
    <script src="{{ mix('js/jquery.js') }}" type="text/javascript" charset="utf-8" async defer></script>
    <script src="{{ mix('js/app.js') }}"></script>

</head>
<body>
    <div class="container">
        <h3>To Do List</h3>
        <form>
    
<input type="text" v-model="job_title">


<button @submit.prevent="addJob">Submit</button>
</form>
        <div class="input-group">
            <input type="text" class="form-control" placeholder="What do you have to do?">
            <span class="input-group-btn">
                <button class="btn btn-default" type="button">Add to List</button>
            </span>
        </div>
        <br>
        <div class="panel panel-primary">
            <div class="panel-heading">
                To Do List
            </div>
            <div class="panel-body">
                <table class="table table-condensed">
                    <thead>
                        <th>
                            Task
                        </th>
                        <th>
                            Option
                        </th>
                    </thead>
                    <tr>
                        <td>
                            Lorem ipsum dolor sit amet, consectetur adipisicing elit. Tempore minus 
                        </td>
                        <td>
                            <a href="" class="btn btn-warning">Edit</a>
                            <a href="" class="btn btn-danger">Delete</a>
                        </td>
                    </tr>

                </table>
            </div>
        </div>
    </div>

</body>
</html>
