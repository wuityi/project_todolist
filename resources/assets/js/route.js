import VueRouter from  'vue-router';

let routes = [
	{

		path : '/',
		name : 'home',
		component : require('./components/TaskList')

	},
	{
		path : '/show',
		name : 'showTask',
		component : require('./components/ShowTask'),
		props: true

	},

	{
		path : '/edit',
		name : 'editTask',
		component : require('./components/EditTaskList'),
		props: true

	}
];


export default new VueRouter({
	routes
});