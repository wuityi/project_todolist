import Vue from 'vue';
import VueRouter from 'vue-router';
import axios from 'axios';
import swal from 'sweetalert';
import VeeValidate from 'vee-validate';
import VueGoodTable from 'vue-good-table';






window.Vue = Vue;
Vue.use(VueRouter);
Vue.use(VeeValidate);
Vue.use(VueGoodTable);
window.axios = axios;


try {
    window.$ = window.jQuery = require('jquery');

    require('bootstrap-sass');
} catch (e) {}



window.axios.defaults.headers.common['X-Requested-With'] = 'XMLHttpRequest';


let token = document.head.querySelector('meta[name="csrf-token"]');

if (token) {
    window.axios.defaults.headers.common['X-CSRF-TOKEN'] = token.content;
} else {
    console.error('CSRF token not found: https://laravel.com/docs/csrf#csrf-x-csrf-token');
}
