import './bootstrap';
import router from './route';



/**
 * Next, we will create a fresh Vue application instance and attach it to
 * the page. Then, you may begin adding components to this application
 * or customize the JavaScript scaffolding to fit your unique needs.
 */

Vue.component('example', require('./components/Example.vue'));
Vue.component('task-list', require('./components/TaskList.vue'));

new Vue({
    el: '#app',
    router
});


